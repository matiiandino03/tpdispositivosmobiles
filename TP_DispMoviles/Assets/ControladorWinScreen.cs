using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControladorWinScreen : MonoBehaviour
{
public void VolverAJugar()
    {
        SceneManager.LoadScene("TestMechanics");
    }

    public void VolverMenu()
    {
        SceneManager.LoadScene("MenuPrincipal");
    }
}
