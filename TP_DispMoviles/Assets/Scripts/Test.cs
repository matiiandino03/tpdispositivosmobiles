using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    public Rigidbody2D rb;

    public bool isUpsideDown;
    public bool isPortrait;
    public static Test instance;

    public GameObject player;

    public delegate void RotationChanged();
    public RotationChanged rotarionChangedY;
    public RotationChanged rotarionChangedX;

    public GameObject playerSprite;

    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        DeviceChange.OnOrientationChange += MyOrientationChangeCode;
        //DeviceChange.OnResolutionChange += MyResolutionChangeCode;
        //DeviceChange.OnResolutionChange += MyOtherResolutionChangeCode;
    }

    private void MyOrientationChangeCode(DeviceOrientation obj)
    {
        if(Input.deviceOrientation == DeviceOrientation.Portrait)
        {
            Quaternion currentRotation = playerSprite.transform.rotation;
            playerSprite.transform.rotation = Quaternion.Euler(0f, 0, 90);
            Debug.Log("Portrait");
            Physics2D.gravity = new Vector2(9.8f, 0);          
            rotarionChangedY.Invoke();
            isPortrait = true;
        }
        if (Input.deviceOrientation == DeviceOrientation.PortraitUpsideDown)
        {
            Quaternion currentRotation = playerSprite.transform.rotation;
            playerSprite.transform.rotation = Quaternion.Euler(0f, 0, -90);
            Debug.Log("PortaitUpSideDown");
            Physics2D.gravity = new Vector2(-9.8f, 0);
            rotarionChangedY.Invoke();
            isPortrait = false;
        }
        if (Input.deviceOrientation == DeviceOrientation.LandscapeLeft)
        {
            Quaternion currentRotation = playerSprite.transform.rotation;
            playerSprite.transform.rotation = Quaternion.Euler(0f, 0, 0);
            Debug.Log("LandScapeLeft");            
            Physics2D.gravity = new Vector2(0, -9.8f);
            isUpsideDown = false;
            rotarionChangedX.Invoke();
        }
        if (Input.deviceOrientation == DeviceOrientation.LandscapeRight)
        {
            Quaternion currentRotation = playerSprite.transform.rotation;
            playerSprite.transform.rotation = Quaternion.Euler(0f, 0, 180);
            Debug.Log("LandScapeRight");
            Physics2D.gravity = new Vector2(0, 9.8f);
            isUpsideDown = true;
            rotarionChangedX.Invoke();
        }
    }

    void Update()
    {
    }
}

