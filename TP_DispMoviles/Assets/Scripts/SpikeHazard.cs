using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeHazard : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.CompareTag("Player"))
        {
            Debug.Log("The Player has died!");
            other.gameObject.transform.position = new Vector3(-9, -4, 0f);
        }
    }

}
