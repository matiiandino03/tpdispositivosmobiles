using System;
using System.Collections;
using UnityEngine;
 
public class DeviceChange : MonoBehaviour 
{
    public static event Action<Vector2> OnResolutionChange;
    public static event Action<DeviceOrientation> OnOrientationChange;
    public static float CheckDelay = 0.5f;        
 
    static Vector2 resolution;                    
    public static DeviceOrientation orientation;        
    static bool isAlive = true;                    
    public static DeviceChange instance;

    private void Awake()
    {
        instance = this;
    }
    void Start() {
        StartCoroutine(CheckForChange());
    }
 
    IEnumerator CheckForChange(){
        resolution = new Vector2(Screen.width, Screen.height);
        orientation = Input.deviceOrientation;
 
        while (isAlive) {
 
            if (resolution.x != Screen.width || resolution.y != Screen.height ) {
                resolution = new Vector2(Screen.width, Screen.height);
                if (OnResolutionChange != null) OnResolutionChange(resolution);
            }
 
            switch (Input.deviceOrientation) {
                case DeviceOrientation.Unknown:            
                case DeviceOrientation.FaceUp:            
                case DeviceOrientation.FaceDown:        
                    break;
                default:
                    if (orientation != Input.deviceOrientation) {
                        orientation = Input.deviceOrientation;
                        if (OnOrientationChange != null) OnOrientationChange(orientation);
                    }
                    break;
            }
 
            yield return new WaitForSeconds(CheckDelay);
        }
    }
 
    void OnDestroy(){
        isAlive = false;
    }
 
}
