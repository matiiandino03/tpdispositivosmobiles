using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class LevelManager : MonoBehaviour
{
public GameObject Level1;
public GameObject Level2;
public GameObject Level3;
public GameObject Level4;
public GameObject Level5;

private void Start()
{
    Level1.SetActive(true);
    Level2.SetActive(false);
    Level3.SetActive(false);
    Level4.SetActive(false);
    Level5.SetActive(false);
}

    private void OnTriggerEnter2D(Collider2D GoalColllider)
    {
        if (GoalColllider.CompareTag("Goal1"))
        {
            AdvanceToLevel2();
            ResetPlayerPosition();
        }

        if (GoalColllider.CompareTag("Goal2"))
        {
            AdvanceToLevel3();
            ResetPlayerPosition();
        }

        if (GoalColllider.CompareTag("Goal3"))
        {
            AdvanceToLevel4();
            ResetPlayerPosition();
        }

        if (GoalColllider.CompareTag("Goal4"))
        {
            AdvanceToLevel5();
            ResetPlayerPosition();
        }

        if (GoalColllider.CompareTag("Goal5"))
        {
            GoToWinScreen();
            ResetPlayerPosition();
        }
    }
    private void ResetPlayerPosition()
    {
        transform.position = new Vector3(-9, -4, 0f);
    }

    private void AdvanceToLevel2()
    {
        Level1.SetActive(false);
        Level2.SetActive(true);
    }

        private void AdvanceToLevel3()
    {
        Level2.SetActive(false);
        Level3.SetActive(true);
    }

        private void AdvanceToLevel4()
    {
        Level3.SetActive(false);
        Level4.SetActive(true);
    }

        private void AdvanceToLevel5()
    {
        Level4.SetActive(false);
        Level5.SetActive(true);
    }

    private void GoToWinScreen()
    {
        Level5.SetActive(false);
        SceneManager.LoadScene("WinScene");
    }
}
