using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerMovement : MonoBehaviour
{
    public Rigidbody2D rb;
    public MovementJoystick movementJoystick;
    public float playerSpeed;

    bool isGrounded;
    public Transform groundCheck;
    public Transform groundCheck2;
    public Transform groundCheck3;
    public Transform groundCheck4;
    float checkRadius = 0.2f;
    public LayerMask mask;

    public bool useY;

    Vector2 JumpDirection;

    private void Start()
    {
        Test.instance.rotarionChangedY += RotationChangedY;
        Test.instance.rotarionChangedX += RotationChangedX;
        movementJoystick.jump += Jump;
    }
    private void FixedUpdate()
    {
        if(!useY)
        {
            if (Test.instance.isUpsideDown)
            {
                isGrounded = Physics2D.OverlapCircle(groundCheck2.position, checkRadius, mask);
                JumpDirection = groundCheck2.transform.up;
            }
            else
            {
                isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, mask);
                JumpDirection = groundCheck.transform.up;
            }
        }
        if(useY)
        {
            if (Test.instance.isPortrait)
            {
                isGrounded = Physics2D.OverlapCircle(groundCheck3.position, checkRadius, mask);
                JumpDirection = groundCheck3.transform.up;
            }
            else
            {
                isGrounded = Physics2D.OverlapCircle(groundCheck4.position, checkRadius, mask);
                JumpDirection = groundCheck4.transform.up;
            }
        }

        if (true)
        {
            if(useY)
            {
                rb.velocity = new Vector2(rb.velocity.x, movementJoystick.joystickVec.y * playerSpeed);
            }
            else
            {
                rb.velocity = new Vector2(movementJoystick.joystickVec.x * playerSpeed, rb.velocity.y);
            }
        }


        
    }

    private void Jump()
    {
        //if(isGrounded)
        //{
        //    rb.AddForce(JumpDirection * 350);
        //}
    }
    public void RotationChangedY()
    {
        useY = true;
    }
    public void RotationChangedX()
    {
        useY = false;
    }
    #region no sirve
    //public void MoveLeft()
    //{
    //    rb.velocity = new Vector2(-5, rb.velocity.y);
    //}

    //public void MoveRight()
    //{
    //    rb.velocity = new Vector2(5, rb.velocity.y);
    //}

    //public void Jump()
    //{
    //    if(isGrounded && !Test.instance.isUpsideDown)
    //    {
    //        rb.velocity = Vector2.up * 5;
    //    }
    //    if (isGrounded && Test.instance.isUpsideDown)
    //    {
    //        rb.velocity = -Vector2.up * 5;
    //    }
    //}
    #endregion
}
