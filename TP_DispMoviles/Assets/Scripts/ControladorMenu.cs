using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControladorMenu : MonoBehaviour
{
   public void Jugar()
    {
        SceneManager.LoadScene("TestMechanics");
    }

    public void Salir()
    {
        Debug.Log("Salir...");
        Application.Quit();
    }
}
